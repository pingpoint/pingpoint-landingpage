package main

import (
	"fmt"
	"log"
	"net/http"

	mailchimp "github.com/RichardKnop/go-mailchimp"
	"github.com/go-playground/validator"
	"github.com/labstack/echo"
)

type (
	//User : sets up email and first name json
	User struct {
		EMAIL string `json:"email" validate:"required,email"`
		FNAME string `json:"fname" validate:"required"`
	}

	//CustomValidator : here you tell us what Salutation is
	CustomValidator struct {
		validator *validator.Validate
	}
)

//Validate  will validate the struct
func (cv *CustomValidator) Validate(i interface{}) error {
	return cv.validator.Struct(i)
}

func main() {
	e := echo.New()
	e.Validator = &CustomValidator{validator: validator.New()}

	e.Static("/public", "public")
	e.File("/", "assets/index.html")

	e.POST("/signup", func(c echo.Context) (err error) {
		u := new(User)
		if err = c.Bind(u); err != nil {
			fmt.Println(err)
			e.Logger.Error(err)
			return
		}
		if err = c.Validate(u); err != nil {
			fmt.Println(err)
			e.Logger.Error(err)
			return
		}
		fmt.Println(u)
		signup(u)
		return c.JSON(http.StatusOK, u)
	})

	e.Logger.Fatal(e.Start(":1323"))
}

//signup  here you tell us what Salutation is
func signup(u *User) {

	log.Printf("%v member", u.EMAIL)
	log.Printf("%v member", u.FNAME)
	params := map[string]interface{}{
		"FNAME": u.FNAME,
	}
	client, err := mailchimp.NewClient("25de54bb5cc34ff5594566e427fdd26d-us15", nil)
	if err != nil {
		//log.Fatal(err)
		log.Printf("%v ERROR", err)
	}

	// Check if the email is already subscribed
	memberResponse, err := client.CheckSubscription(
		"e850e6786d",
		u.EMAIL,
	)

	// User is already subscribed, update the subscription
	if err == nil {
		memberResponse, err = client.UpdateSubscription(
			"e850e6786d",
			u.EMAIL,
			params,
		)

		if err != nil {
			// Check the error response
			errResponse, ok := err.(*mailchimp.ErrorResponse)

			// Could not type assert error response
			if !ok {
				//log.Fatal(err)
				log.Printf("%v ERROR", err)
			}

			//log.Fatal(errResponse)
			log.Printf("%v ERROR", errResponse)
		}

		log.Printf(
			"%s's subscription has been updated. Status: %s",
			memberResponse.EmailAddress,
			memberResponse.Status,
		)
		//return c.Redirect(routes.App.Index())
	}

	if err != nil {
		// Check the error response
		errResponse, ok := err.(*mailchimp.ErrorResponse)

		// Could not type assert error response
		if !ok {
			//log.Fatal(err)
			log.Printf("%v ERROR", err)
		}

		// 404 means we can process and subscribe user,
		// error other than 404 means we return error
		if errResponse.Status != http.StatusNotFound {
			//log.Fatal(errResponse)
			log.Printf("%v ERROR", errResponse)
		}
	}

	// Subscribe the email
	memberResponse, err = client.Subscribe(
		"e850e6786d",
		u.EMAIL,
		params,
	)

	if err != nil {
		// Check the error response
		errResponse, ok := err.(*mailchimp.ErrorResponse)

		// Could not type assert error response
		if !ok {
			//log.Fatal(err)
			log.Printf("%v ERROR", err)
		}

		//log.Fatal(errResponse)
		log.Printf("%v ERROR", errResponse)
	}

	log.Printf(
		"%s has been subscribed successfully. Status: %s",
		memberResponse.EmailAddress,
		memberResponse.Status,
	)
	// return c.Redirect(routes.App.Index())

}
