var postServices = angular.module("postServices", []);

postServices.factory('PostForm', ['$http',
    function($http) {
        return {
            Post: function(member) {
                return $http({
                    url: '/signup',
                    method: 'POST',
                    data: member,
                    headers: {
                        'CommandType': 'CreateTask',
                        'ContentType': 'application/json'
                    }
                });
            }
        };
    }
]);